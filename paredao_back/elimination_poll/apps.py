from django.apps import AppConfig


class EliminationPollConfig(AppConfig):
    name = 'elimination_poll'
