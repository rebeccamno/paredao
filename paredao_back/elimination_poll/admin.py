from django.contrib import admin
from .models import Contestant, EliminationPoll, EliminationPollContestant, Vote

class EliminationPollContestantInline(admin.TabularInline):
    model = EliminationPollContestant


class EliminationPollAdmin(admin.ModelAdmin):
    inlines = [
        EliminationPollContestantInline,
    ]

admin.site.register(Vote)
admin.site.register(Contestant)
admin.site.register(EliminationPoll, EliminationPollAdmin)
