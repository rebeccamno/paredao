# Generated by Django 3.1.2 on 2020-10-19 19:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contestant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='EliminationPoll',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('initial_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='EliminationPollContestant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('elimination_phone_number', models.CharField(max_length=20)),
                ('elimination_sms_number', models.CharField(max_length=20)),
                ('contestant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elimination_poll.contestant')),
                ('elimination_poll', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elimination_poll.eliminationpoll')),
            ],
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('contestant_elimination_poll', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='elimination_poll.eliminationpollcontestant')),
            ],
        ),
        migrations.AddField(
            model_name='eliminationpoll',
            name='contestants',
            field=models.ManyToManyField(through='elimination_poll.EliminationPollContestant', to='elimination_poll.Contestant'),
        ),
    ]
