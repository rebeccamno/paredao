from django.urls import path

from .views import (
    EliminationPollList,
    VoteCreate
)

urlpatterns = [
    path('', EliminationPollList.as_view(), name='eliminationpoll'),
    path('vote/', VoteCreate.as_view(), name='eliminationpoll_vote'),
]
