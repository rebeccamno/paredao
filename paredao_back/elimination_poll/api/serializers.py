from rest_framework import serializers
from elimination_poll.models import (Contestant, EliminationPoll,
    EliminationPollContestant, Vote)


class ContestantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contestant
        fields = ['name', 'image']


class EliminationPollContestantSerializer(serializers.ModelSerializer):
    contestant = ContestantSerializer(many=False, read_only=True)

    class Meta:
        model = EliminationPollContestant
        fields = ['id','contestant', 'elimination_phone_number', 'elimination_sms_number']


class EliminationPollSerializer(serializers.ModelSerializer):
    contestants =  serializers.SerializerMethodField()

    class Meta:
        model = EliminationPoll
        fields = ['initial_date', 'end_date', 'contestants']

    def get_contestants(self, obj):
        qset = EliminationPollContestant.objects.filter(elimination_poll=obj)

        return [EliminationPollContestantSerializer(c).data for c in qset]


class VoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vote
        fields = ['contestant_elimination_poll']
