from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView
from elimination_poll.models import EliminationPoll, Vote
from .serializers import EliminationPollSerializer, VoteSerializer
from rest_framework.response import Response


class VoteCreate(CreateAPIView):
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer

class EliminationPollList(APIView):
    def get(self, request, format=None):
        eliminationpoll = EliminationPoll.objects.last()

        return Response(EliminationPollSerializer(eliminationpoll,
                                                  many=False,
                                                  context={'request': request}
                                                 ).data)
