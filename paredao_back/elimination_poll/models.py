from django.db import models

# Create your models here.


class Contestant(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField()

    def __str__(self):
        return self.name

class EliminationPoll(models.Model):
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    initial_date = models.DateTimeField()
    end_date = models.DateTimeField()
    contestants = models.ManyToManyField(
        Contestant,
        through='EliminationPollContestant',
        through_fields=('elimination_poll', 'contestant'),
    )

    def __str__(self):
        return self.name

class EliminationPollContestant(models.Model):
    elimination_poll = models.ForeignKey(EliminationPoll,
                                         on_delete=models.CASCADE)
    contestant = models.ForeignKey(Contestant, on_delete=models.CASCADE)
    elimination_phone_number = models.CharField(max_length=20)
    elimination_sms_number = models.CharField(max_length=20)


class Vote(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    contestant_elimination_poll = models.ForeignKey(EliminationPollContestant,
                                                    on_delete=models.CASCADE)

    def __str__(self):
        return self.contestant_elimination_poll.contestant.name
