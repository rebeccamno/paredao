import React from 'react';
import {PageContainer, EliminationPoll} from '../../components'


function Home() {
   
    return (
      <PageContainer>
        <EliminationPoll />
      </PageContainer>
    );
}

export default Home;