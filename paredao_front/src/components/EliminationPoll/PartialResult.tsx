import React from 'react';
import { PieChart } from "react-minimal-pie-chart";
import { IContestant } from "../ContestantBox/index"
import {Countdown} from '../../components';
import {
  PartialResultText,
  PartialResultContent,
  PartialResultContestantImageWrapper,
  ChartWrapper,
  CountdownWrapper,
} from "./EliminationPollStyled";


interface IPartialResult{
  votedContestant: IContestant,
  partialResult: any,
  endDate: Date
}

interface IContestantPartialResult extends IContestant{
  percent: number,
  color: string 
}

function PartialResult(props: IPartialResult){
    const { votedContestant, partialResult, endDate } = props;

    const defaultLabelStyle = {
      fontFamily: "sans-serif",
      fill: "#fff",
      fontSize: "0.4rem",
      fontWeight: 600,
      textShadow: "1px 1px 2px rgba(150, 150, 150, 1)"
    };

    return (
      <>
        <PartialResultText>
          <b>Parabéns!</b> Seu voto para <b>{votedContestant.name}</b> foi
          enviado com sucesso.
        </PartialResultText>

        <PartialResultContent>
          {partialResult.map((contestant: IContestant) => {
            return (
              <PartialResultContestantImageWrapper key={contestant.id}>
                <img src={contestant.image_url} alt={contestant.name} />
              </PartialResultContestantImageWrapper>
            );
          })}
          <ChartWrapper>
            <PieChart
              data={partialResult.map((contestant: IContestantPartialResult) => {
                return { value: contestant.percent, color: contestant.color };
              })}
              lineWidth={30}
              totalValue={100}
              paddingAngle={1}
              labelStyle={{
                ...defaultLabelStyle,
              }}
              label={({ dataEntry }) => Math.round(dataEntry.percentage) + "%"}
              labelPosition={100 - 30 / 2}
              startAngle={120}
              lengthAngle={300}
              viewBoxSize={[100, 50]}
            />
            <CountdownWrapper>
              <Countdown endDate={endDate} />
            </CountdownWrapper>
          </ChartWrapper>
        </PartialResultContent>
      </>
    );

}

export { PartialResult }