import styled from "styled-components";
import Sprite from "../../assets/images/sprite.png";
import Dots from "../../assets/images/dots.png";


const EliminationPollWrapper = styled.div`
    border: 1px solid #d6d6d6;
    box-shadow: 0px 0px 0px 6px rgba(229,229,229, 0.9);
    border-radius: 2px;
    display: flex;
    flex-direction: column;
    position: relative;
`;

const CloseButton = styled.button`
    background-image: url(${Sprite});
    width: 40px;
    height: 40px;
    display: inline-block;
    margin-right: 10px;
    position: absolute;
    right: -37px;
    top: -22px;
    background-position: -0 -100px;
    background-repeat: no-repeat;
    background-color: transparent;
    cursor: pointer;
`;

const ContestantsWrapper = styled.div`
    display: flex;
    column-gap: 20px;
    justify-content: center;
    @media(max-width: 800px) {
        row-gap: 20px;
        flex-direction: column;
    }
`;

const EliminationPollHeader = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 20px 20px 10px 20px;
    @media(max-width: 800px){
        padding: 20px 0 10px 0;
    }
    &::before{
        content: "";
        background-image: url(${Sprite});
        width: 38px;
        height: 38px;
        display: inline-block;
        margin-right: 10px;
    }
`;

const EliminationPollTitle = styled.h1`
    font-family: "Raleway", sans-serif;
    font-weight: 400;
    text-transform: uppercase;
    font-size: 1.4rem;
    color: #545454;
    letter-spacing: -1.1px;

    @media(max-width: 800px) {
        font-size: 1.1rem;
    }
`
const Divider = styled.div`
    background-image: url(${Dots});
    height: 9px;
    margin: 0 20px;
    padding: 0 20px;
`;

const EliminationPollFooter = styled.div`
    background-color: #f5f5f5;
    border-top: 1px solid #d6d6d6;
    padding: 20px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const EliminationPollContent = styled.div`
    padding: 20px;
    @media(max-width: 800px) {
        padding: 20px 10px;
    }
`;

const PartialResultText = styled.h1`
    font-size: 1.2rem;
    font-weight: 400;
    color: #545454;
    padding: 20px;
`;

const PartialResultContent = styled.div`
    display: flex;
    overflow: hidden;
    justify-content: space-between;
`;

const PartialResultContestantImageWrapper = styled.div`
    margin-right: -50px;
    order: 3;
    display: flex;
    img{
        margin-top: auto;
    }
    &:first-child{
        order: 1;
        margin-left: -50px;
        margin-right: 0px;
    }
`;

const ChartWrapper = styled.div`
    width: 265px;
    order: 2;
    margin: 0 -30px 0 -30px;
    z-index: 10;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
`;

const ErrorText = styled.div`
    color: #545454;
    margin-bottom: 10px;
`

const CountdownWrapper = styled.div`
    position: absolute;
    bottom: 40px;
`

const CaptchaWrapper = styled.div`
    margin-top: 10px;
    display: flex;
    align-items: center;
    justify-content: center;
`

export {
  EliminationPollWrapper,
  ContestantsWrapper,
  EliminationPollTitle,
  EliminationPollHeader,
  EliminationPollContent,
  EliminationPollFooter,
  CloseButton,
  Divider,
  PartialResultText,
  PartialResultContent,
  PartialResultContestantImageWrapper,
  ChartWrapper,
  ErrorText,
  CountdownWrapper,
  CaptchaWrapper,
};