import React, { useState } from "react";
import ReCAPTCHA from "react-google-recaptcha";
import { IContestant} from "../ContestantBox/index"
import { ContestantBox, Button } from "../../components";
import { PartialResult } from "./PartialResult";
import { eliminationPollMockedData } from "../../mockedData/eliminationPollMockedData";
import { partialResult } from "../../mockedData/partialResult";

import {
  EliminationPollWrapper,
  CloseButton,
  ContestantsWrapper,
  EliminationPollTitle,
  EliminationPollHeader,
  EliminationPollFooter,
  EliminationPollContent,
  Divider,
  ErrorText,
  CaptchaWrapper,
} from "./EliminationPollStyled";


function EliminationPoll(){
    const [captchaToken, setCaptchaToken] = useState<string | null>(null)
    const [selectedContestant, setSelectedContestant] = useState<IContestant | null>(null);
    const [registeredVote, setRegisteredVote] = useState(false);
    const [error, setError] = useState<string | null>(null);
    
    function onChange(token: string | null) {
      setCaptchaToken(token)
    }
  
    const selectContestant = (contestant: IContestant) => {
      setSelectedContestant(contestant);
    };

    const handleVote = () => {
      if (!captchaToken){
        return
      }
      if (!selectedContestant) {
        setError("Para votar selecione o participante que deseja eliminar.");
        return;
      }
      // Chamada da API para registrar o voto
      setRegisteredVote(true);
    }

    
    return (
      <EliminationPollWrapper>
        <CloseButton />
        <EliminationPollHeader>
          <EliminationPollTitle>
            {"Quem deve ser "} <b>eliminado</b> {" ? "}
          </EliminationPollTitle>
        </EliminationPollHeader>
        <Divider />
        {!registeredVote ? (
          <>
            <EliminationPollContent>
              {error && <ErrorText>{error}</ErrorText>}
              <ContestantsWrapper>
                {eliminationPollMockedData.contestants.map((contestant) => {
                  return (
                    <ContestantBox
                      key={contestant.id}
                      contestant={contestant}
                      isSelected={selectedContestant?.id === contestant.id}
                      selectContestant={() => {
                        selectContestant(contestant);
                      }}
                    />
                  );
                })}
              </ContestantsWrapper>
              <CaptchaWrapper>
                <ReCAPTCHA
                  sitekey={process.env.REACT_APP_RECAPTCHA_KEY ? process.env.REACT_APP_RECAPTCHA_KEY: ''  }
                  onChange={onChange}
                />
              </CaptchaWrapper>
            </EliminationPollContent>
            <EliminationPollFooter>
              <Button
                onClick={() => {
                  handleVote();
                }}
              >
                Envie seu voto agora
              </Button>
            </EliminationPollFooter>
          </>
        ) : (
          <>
              {selectedContestant ?
                <PartialResult
                  votedContestant={selectedContestant}
                  partialResult={partialResult}
                  endDate={eliminationPollMockedData.closeDateTime}
                />
            : ""  
            }
          
          </>
        )
      }
      </EliminationPollWrapper>
    );
}

export {EliminationPoll}
