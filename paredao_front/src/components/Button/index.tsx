import React, {ReactNode} from 'react'
import {ButtonWrapper} from './ButtonStyled'

interface IButton{
    children: ReactNode,
    onClick: ()=> void,
}

function Button(props: IButton){
    const {children} = props;

    return (
        <ButtonWrapper {...props}>
            {children}
        </ButtonWrapper>
    )
}

export { Button } ;