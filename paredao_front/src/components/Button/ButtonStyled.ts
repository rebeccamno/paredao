import styled from 'styled-components'

const ButtonWrapper = styled.button`
    height: 40px;
    color: #FFF;
    background: linear-gradient(180deg, rgba(33,154,251,1) 0%, rgba(19,112,194,1) 80%);
    background: linear-gradient(to bottom, rgba(39,153,251,1) 0%, rgba(23,115,195,1) 100%);
    box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.55);
    border-radius: 3px;
    font-weight: 600;
    border: 1px solid #1089ea;
    padding: 0 30px;
    cursor: pointer;
    transition: all 300ms;

    &:hover{
        opacity: 0.8;
    }
`;


export { ButtonWrapper };