import React, {ReactNode} from 'react'
import {PageContainerWrapper} from './PageContainerStyled'


interface IPageContainer{
    children: ReactNode
}

function PageContainer(props: IPageContainer){
    const { children } = props

    return (
        <PageContainerWrapper>
            {children}
        </PageContainerWrapper>
    )
}

export { PageContainer }