import styled from 'styled-components'

const PageContainerWrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    min-height: 100vh;

    @media(max-width: 800px) {
        padding: 30px 20px
    }
`;

export { PageContainerWrapper };