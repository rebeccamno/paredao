import styled from 'styled-components';

const Text = styled.div`
    color: #a9a9a9;
    font-size: 0.7rem;
    line-height: 1.5;
    text-transform: uppercase;
`

const CountdownTime = styled.div`
    color: #f59637;
    font-size: 2rem;
`

const CountdownWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    .day {
        color: #f59637;
        font-size: 0.8rem;
        text-transform: uppercase;
    }
`;

export { Text, CountdownTime, CountdownWrapper };