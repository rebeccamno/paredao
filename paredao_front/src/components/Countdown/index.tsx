import React, { useEffect, useState } from "react";
import { Text, CountdownTime, CountdownWrapper } from './CountdownStyled';


interface CountdownProps {
  finishedAction?: () =>void | null,
  endDate: Date
}

interface ICountdown {
  days: number,
  hours: number,
  min: number
  sec: number
}


const Countdown = (props: CountdownProps) => {
  const finishedCountdownHandler = props.finishedAction
    ? props.finishedAction
    : () => {};
  const [countDown, setCountDown] = useState<ICountdown | null>(null);
  const [loaded, setLoaded] = useState<boolean>(false);
  const [finished, setFinished] = useState<boolean | null>(null);
  const endDate = props.endDate

  useEffect(() => {
    const date = calculateCountdown(endDate);
    setLoaded(true);
    if (date) {
      setCountDown(date);
    }

    const interval = setInterval(() => {
      const date = calculateCountdown(endDate);
      date ? setCountDown(date) : clearInterval(interval);
    }, 1000);
    return () => clearInterval(interval);
    //eslint-disable-next-line
  }, []);

  const finishedCountdown = () => {
    finishedCountdownHandler();
    setFinished(true);
  };

  const calculateCountdown = (endDate: Date) => {
      const currentDate = new Date();

      const totalSeconds = (endDate.getTime() - currentDate.getTime()) / 1000;

      // clear countdown when date is reached
      if (totalSeconds <= 0) {
        finishedCountdown();
        return false;
      }
      
      const days = Math.floor(totalSeconds / 3600 / 24);
      const hours = Math.floor(totalSeconds / 3600) % 24;
      const mins = Math.floor(totalSeconds / 60) % 60;
      const seconds = Math.floor(totalSeconds) % 60;

      const timeLeft = {
        days: days,
        hours: hours,
        min: mins,
        sec: seconds,
      };
      return timeLeft;
  };
  const addLeadingZeros = (value: number) => {
    let newValue = String(value);
    while (newValue.length < 2) {
      newValue = "0" + newValue;
    }
    return newValue;
  };

  return (
    <CountdownWrapper>
      <Text>Faltam</Text>
      {countDown && !finished && (
        <>
          {countDown.days > 0 && (
            <div className="day">
              {countDown.days} {countDown.days === 1 ? "Dia" : "Dias"}
            </div>
          )}
          <CountdownTime>
            {`${addLeadingZeros(countDown.hours)}:${addLeadingZeros(countDown.min)}:${addLeadingZeros(countDown.sec)}`}
          </CountdownTime>
        </>
      )}
      <Text>Para encerrar a votação</Text>
    </CountdownWrapper>
  );
};

export { Countdown };
