import styled from "styled-components";


const ContestantBoxWrapper = styled.div`
    h1{
        color: #555;
        font-size: 1.2rem;
        line-height: 1.7;
    }
`;

const ImageBox = styled.div`
    border: 1px solid #e0e0e0;
    border-radius: 3px;
    width: 280px;
    height: 290px;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    cursor: pointer;

    &.active{
        box-shadow:inset 0px 0px 0px 5px #ff9a00;
    }
    img{
        z-index: -1;
        max-width: 100%;
        margin-top: auto;
    }
`;

const Text = styled.div`
    font-size: 0.74rem;
    color: #888;
    margin-top: 5px;
    line-height: 1.8;
    b{
        color: #555;
    }
`

export { ContestantBoxWrapper, ImageBox, Text };