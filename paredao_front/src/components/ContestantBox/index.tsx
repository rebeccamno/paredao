import React from 'react';
import { ContestantBoxWrapper, ImageBox, Text } from './ContestantBoxStyled';


export interface IContestant {
  id: number,
  name: string,
  image_url: string,
  elimination_phone_number: string,
  elimination_sms_number: string
}

interface ContestantBoxProps {
  contestant: IContestant,
  isSelected: boolean,
  selectContestant: (id: number) => void
}

function ContestantBox(props: ContestantBoxProps){
    const { contestant, isSelected, selectContestant } = props
    
    return (
      <ContestantBoxWrapper>
        <h1>{contestant.name}</h1>
        <ImageBox
          className={isSelected ? "active" : ""}
          onClick={() => {
            selectContestant(contestant.id);
          }}
        >
          <img src={contestant.image_url} alt={contestant.name}></img>
        </ImageBox>
        <Text>
          {"Para eliminar o"} <b>{contestant.name}</b> {"pelo telefone "} <br/> {"disque "}
          <b>{contestant.elimination_phone_number}</b>{" "}
          {" ou mande um SMS para "}
          <b>{contestant.elimination_sms_number}</b>
        </Text>
      </ContestantBoxWrapper>
    );

}

export {ContestantBox};