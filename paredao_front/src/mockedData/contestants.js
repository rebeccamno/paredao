import Contestant1Image from "../assets/images/contestant1.png";
import Contestant2Image from "../assets/images/contestant2.png";


const contestants = [
    {
        id: 1,
        name: "Participante 1",
        image_url: Contestant1Image,
        elimination_phone_number: "0800-123-001",
        elimination_sms_number: "8001",
    },
    {
        id: 2,
        name: "Participante 2",
        image_url: Contestant2Image,
        elimination_phone_number: "0800-123-002",
        elimination_sms_number: "8002",
    }
];


export {contestants}