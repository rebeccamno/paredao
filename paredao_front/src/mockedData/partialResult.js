import { contestants } from './contestants'

const partialResult = [
  {...contestants[0], percent: 30, color: "#c7c7c7" },
  {...contestants[1], percent: 70, color: "#f59637" },
];

export { partialResult }
