import {contestants} from './contestants'

const eliminationPollMockedData = {
    closeDateTime: new Date(2020, 9, 20, 21, 0, 0),
    contestants: contestants
}

export { eliminationPollMockedData };