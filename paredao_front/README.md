
Desafio PayTV WEB

  

**Globo.com: coding challenge**

  

##  Instalação

  Clone esse repositório. Você precisa ter o `node` e `npm` instalados na sua máquina.

```
npm run install
```

##  Inicialização 
Para inicializar o projeto rode
```
npm run start
```

## Docker
Esse projeto também pode ser inicializado via docker.
```
docker-compose up
```

## Tecnologias
As seguintes ferramentas foram usadas na construção do projeto:

- [React](https://pt-br.reactjs.org/)